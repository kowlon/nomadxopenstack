job "registry" {
  datacenters = ["dc1"]

  group "app" {
    network {
      port "app" {
        to = 5000
	static = 5000
      }
    }

    task "app" {
      driver = "docker"

      config {
        image = "registry"
        ports = ["app"]
      }

      resources {
        cpu    = 512
        memory = 1024
      }
    }
  }
}