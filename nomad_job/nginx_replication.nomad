job "nginx-replication" {
  datacenters = ["dc1"]

  group "app" {
    count = 5
    network {
      port "app" {
        to = 80
      }
    }

    task "app" {
      driver = "docker"

      config {
        image = "nginx"
        ports = ["app"]
      }

      resources {
        cpu    = 100
        memory = 100
      }
    }
  }
}