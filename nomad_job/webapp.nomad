job "webapp" {
  datacenters = ["dc1"]

  group "app" {
    network {
      port "app" {
        to = 8080
	static = 8080
      }
    }

    task "app" {
      driver = "docker"

      config {
        image = "172.24.4.195:5000/webapp"
        ports = ["app"]
      }

      resources {
        cpu    = 100
        memory = 100
      }
    }
  }
}