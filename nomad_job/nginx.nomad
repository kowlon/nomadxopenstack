job "nginx" {
  datacenters = ["dc1"]

  group "app" {
    network {
      port "app" {
        to = 80
	static = 8080
      }
    }

    task "app" {
      driver = "docker"

      config {
        image = "nginx"
        ports = ["app"]
      }

      resources {
        cpu    = 100
        memory = 100
      }
    }
  }
}