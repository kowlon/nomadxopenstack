#!/bin/bash
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts
CONSUL_SERVER=172.24.4.195
NOMAD_SERVER=172.24.4.168
NOMAD_VERSION=1.3.0
CONSUL_VERSION=1.12.0

LOCAL_IP=`curl http://169.254.169.254/latest/meta-data/public-ipv4`

echo "Setting up DNS"
sudo rm -rf /etc/resolv.conf
sudo bash -c 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'
sudo chattr +i /etc/resolv.conf

echo "Setting up Docker"
sudo apt-get update
sudo apt-get install apt-transport-httpsca-certificates curl gnupg-agent software-properties-common -y
sudo apt-get install unzip -y

wget -O get-docker.sh https://get.docker.com/

chmod +x get-docker.sh
./get-docker.sh

echo "Installing Nomad..."


curl -sSL https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip -o nomad.zip
unzip nomad.zip
sudo install nomad /usr/bin/nomad
sudo mkdir -p /etc/nomad/config
sudo chmod -R a+w /etc/nomad

echo "Installing Consul..."
curl -sSL https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip > consul.zip
unzip consul.zip
sudo install consul /usr/bin/consul
sudo mkdir -p /etc/consul
sudo chmod a+w /etc/consul
sudo mkdir -p /etc/consul/data
sudo chmod a+w /etc/consul/data
sudo mkdir -p /etc/consul/config
sudo chmod a+w /etc/consul/config
HOSTNAME=`hostname`

cat > /etc/nomad/config/client.hcl <<EOF
bind_addr = "0.0.0.0"
log_level = "DEBUG"
data_dir = "/etc/nomad"
datacenter = "dc1"
name = "$HOSTNAME"
client {
  enabled = true
  server_join {
    retry_join = ["$NOMAD_SERVER"]
  }
  
  reserved {
    reserved_ports = "22,53,68,111,8301,8500,8600"
  }
  
  "options" = {
      "driver.raw_exec.enable" = "1"
      "docker.privileged.enabled" = "true"
    }
  
  host_volume "dockersock" {
    path      = "/var/run/docker.sock"
    read_only = false
  }
}
addresses {
  rpc  = "$LOCAL_IP"
  serf = "$LOCAL_IP"
}
advertise {
  http = "$LOCAL_IP:4646"
}

telemetry {
  collection_interval = "1m"
  prometheus_metrics = true
  publish_allocation_metrics = true
  publish_node_metrics = false
}

EOF
cat > /etc/nomad/config/plugin.hcl <<EOF
plugin "docker" {
  config {
    endpoint = "unix:///var/run/docker.sock"

    volumes {
      enabled      = true
      selinuxlabel = "z"
    }

    allow_privileged = true
    allow_caps       = ["ALL"]
  }
}

EOF

sudo bash -c 'cat > /etc/systemd/system/nomad.service <<EOF
[Unit]
Description=Nomad
Requires=network-online.target
After=network-online.target

[Service]
Restart=on-failure
ExecStart=/usr/bin/nomad agent -config=/etc/nomad/config
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity

[Install]
WantedBy=multi-user.target
EOF'

sudo systemctl enable nomad
sudo systemctl start nomad

cat > /etc/consul/config/client.json <<EOF
{
  "server": false,
  "ui": true,
  "data_dir": "/etc/consul/data",
  "client_addr": "0.0.0.0",
  "datacenter": "dc1",
  "advertise_addr": "$LOCAL_IP",
  "retry_join": ["$CONSUL_SERVER"]
}
EOF
cat > /etc/consul/config/connect.hcl <<EOF
connect {
  enabled = true
}
ports {
  grpc = 8502
}
EOF
cat > /etc/consul/config/ports.json <<EOF
{
  "ports": {
    "dns": 553
  }
}
EOF

sudo bash -c 'cat > /etc/systemd/system/consul.service <<EOF
[Unit]
Description=Consul
Requires=network-online.target
After=network-online.target

[Service]
Restart=on-failure
ExecStart=/usr/bin/consul agent -config-dir=/etc/consul/config
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
RestartSec=30
StartLimitBurst=5

[Install]
WantedBy=multi-user.target
EOF'
sudo systemctl enable consul
sudo systemctl start consul