#get auth
source ~/devstack/openrc admin admin

#fix error pyton
pip install --upgrade cryptography

#generate key
ssh-keygen -f devops -P ""
openstack keypair create --public-key devops.pub devopskey

#create subnet network
openstack network create devops
openstack subnet create --subnet-range 192.168.9.0/24 --network devops devopssubnet
openstack router create devopsrouter
openstack router set --external-gateway public devopsrouter
openstack router add subnet devopsrouter devopssubnet

#create SG
openstack security group create devops
openstack security group rule create --ingress devops

#create resource
openstack server create --flavor ds2G --image $(openstack image list | grep -F Ubuntu-20.04 | cut -f3 -d '|') --nic net-id=$(openstack network list | grep devops | cut -f2 -d '|' | tr -d ' ') --security-group devops --key-name devopskey nomad_consul_server
openstack server create --flavor ds2G --image $(openstack image list | grep -F Ubuntu-20.04 | cut -f3 -d '|') --nic net-id=$(openstack network list | grep devops | cut -f2 -d '|' | tr -d ' ') --security-group devops --key-name devopskey nomad_server
openstack server create --flavor ds2G --image $(openstack image list | grep -F Ubuntu-20.04 | cut -f3 -d '|') --nic net-id=$(openstack network list | grep devops | cut -f2 -d '|' | tr -d ' ') --security-group devops --key-name devopskey nomad_consul_worker
openstack server create --flavor ds2G --image $(openstack image list | grep -F Ubuntu-20.04 | cut -f3 -d '|') --nic net-id=$(openstack network list | grep devops | cut -f2 -d '|' | tr -d ' ') --security-group devops --key-name devopskey nomad_consul_worker_2
openstack server create --flavor ds2G --image $(openstack image list | grep -F Ubuntu-20.04 | cut -f3 -d '|') --nic net-id=$(openstack network list | grep devops | cut -f2 -d '|' | tr -d ' ') --security-group devops --key-name devopskey gitlab
